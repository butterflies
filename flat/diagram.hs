import Diagrams.Prelude
import Diagrams.Backend.Cairo.CmdLine (Cairo, defaultMain)

import Control.Monad (forM_)
import System.Environment (withArgs)

main :: IO ()
main = do
  forM_ [2 .. 5] $ \s -> forM_ [0 .. s `div` 2] $ \q -> do
    let p = s - q
        n = p * p + q * q + p * q
    withArgs ["-o", "colouring-" ++ (if n < 10 then "0" else "") ++ show n ++ "-" ++ show p ++ "-" ++ show q ++ ".png"] $
      defaultMain (diagram p q)

diagram :: Integer -> Integer -> QDiagram Cairo R2 Any
diagram p q = dia
  where
      n = p * p + q * q + p * q
      i = p `gcd` q
      j = n `div` i
      o = head [ o' | o' <- [0 .. j - 1], (p + q) `mod` j == (o' * i * (q `div` i)) `mod` j ]
      s = 32
      t = s * sqrt 3 / 2
      x = -100
      y = -50
      w = 200
      h = 100
      pt u v = p2 (s * (fromInteger u + fromInteger v / 2), t * fromInteger v)
      rt u v = r2 (s * (fromInteger u + fromInteger v / 2), t * fromInteger v)
      triangle u v a b = close $ fromVertices [ pt u v, pt (u + b) (v + a), pt (u - a) (v + b + a) ]
      grid = mconcat [ triangle (u - z) v 1 0 | v <- [y .. y + h], let z = (v + 1) `div` 2, u <- [x .. x + w] ]
      pargram u v = translate (r2(-3*s/4, -t/2)) $ close $ fromVertices [ pt u v, pt (u + j) v, pt (u + j) (v + i), pt u (v + i) ]
      isV u v = local u v == (0, 0)
      local u v =
        let d = i * (v `div` i)
        in  ((u + o * d) `mod` j, v `mod` i)
      border uv d =
        let xx = strutX (uv * s)
            yy = strutY (uv * t)
        in  beside (r2 (0,-1)) (beside (r2(0,1)) (beside (r2(-1,0)) (beside (r2(1,0)) d xx) xx) yy) yy
      (uu, vv) = local 1 (-1)
      pp = 1 - uu
      qq = -1 - vv
      dia0 = mconcat
        [ stroke (fromVertices [pt 0 0, pt (-p) 0, pt (-(p + q)) q]) # lc black # lw 3 # border 5
        , stroke (triangle 0 0 p q)  # fc red  # opacity 0.5 # border 5
        , stroke (pargram pp qq) # fc blue # opacity 0.5 # border 5
        ]
      dia = mconcat
        [ mconcat [ stroke (translate (rt u v) $ circle (s / 4)) # lc black # lw 1 | v <- [y .. y + h], u <- [x .. x + w], isV u v ]
        , mconcat [ translate (rt u v) $ circle (s / 4) | u <- [pp .. pp + j - 1 ], v <- [qq .. qq + i - 1] ] # fc yellow
        , dia0
        , mconcat [ stroke (triangle u v p q) # lc red  # lw 1 | v <- [y .. y + h], u <- [x .. x + w], isV u v ]
        , mconcat [ stroke (pargram u v)      # lc blue # lw 1 | v <- [y .. y + h], u <- [x .. x + w], isV u v ]
        , stroke grid # lc green # lw 1
        , rect (s * fromIntegral w) (t * fromIntegral h) # fc white
        ] # withEnvelope dia0 # centerXY # withEnvelope (rect 1024 576 `asTypeOf` dia)
