{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}
module Geometry.Class where

class Geometry point geodesic | point -> geodesic, geodesic -> point where
  distance :: point -> point -> Double
  angle :: point -> point -> point -> Double
  geodesic :: point -> point -> geodesic
  rotate :: Double -> point -> point -> point
  translate :: Double -> geodesic -> point -> point
  midpoint :: point -> point -> point
  midpoint p q = translate (distance p q / 2) (geodesic p q) p

class Embedding model point where
  embed :: point -> model
  model :: model -> Maybe point
