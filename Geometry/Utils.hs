module Geometry.Utils
  ( fixAngle
  ) where

import Data.Fixed (mod')

fixAngle :: Double -> Double
fixAngle a =
  let b = a `mod'` (2 * pi)
  in  if b > pi then b - 2 * pi else b
