module Trapezium
  ( Trapezium(..)
  , trapezium
  , origin
  , neighbours
  , toTriangles
  ) where

import Data.List (partition)

import HalfPlane
import Triangle

data Trapezium = Trapezium{ bTL, bTM, bTR, bBR, bBL, bFR, bFL :: Point, bSpots, bWings, bBody :: Int }
  deriving Show

trapezium :: Trapezium
trapezium = Trapezium tl tm tr br bl fr fl (-1) (-1) (-1)
  where
    tl = Point 0 1
    t  = fromPointAngle tl ( pi / 7)
    l  = fromPointAngle tl (-pi / 7)
    d  = fromPointAngle tl 0
    tm = atDist t tl (1 * s / 2) True
    tr = atDist t tl (2 * s / 3) True
    fr = atDist t tl (3 * s / 3) True
    fl = atDist l tl (3 * s / 3) True
    bl = atDist l tl (    s / 3) True
    br = atDist d tl r True
    s = acosh ((cos (2 * pi / 7) + cos (2 * pi / 7) ^ two) / sin (2 * pi / 7) ^ two)
    r = asinh (sinh s * sin (pi / 7) / sin (2 * pi / 3))
    two :: Int
    two = 2

origin :: Trapezium -> Point
origin = bBL

neighbours :: Trapezium -> [Trapezium]
neighbours z =
  [ (rotateAbout' (bTL z) (2 * pi / 7) z){ bTL = bTL z }
  , (rotateAbout' (bTM z)      pi      z){ bTM = bTM z }
  , (rotateAbout' (bBR z) (2 * pi / 3) z){ bBR = bBR z }
  ]

rotateAbout' :: Point -> Double -> Trapezium -> Trapezium
rotateAbout' p a b = b
  { bTL = rotateAbout p a (bTL b)
  , bTM = rotateAbout p a (bTM b)
  , bTR = rotateAbout p a (bTR b)
  , bBR = rotateAbout p a (bBR b)
  , bBL = rotateAbout p a (bBL b)
  , bFR = rotateAbout p a (bFR b)
  , bFL = rotateAbout p a (bFL b)
  }

toTriangles :: Trapezium -> [Triangle]
toTriangles (Trapezium tl _ tr br bl _ _ c0 c1 c2) =
  [ Triangle tl tm bl (t 0   1) (t 0.5  1) (t 0.25 0) h0 h1 h2
  , Triangle tm tr br (t 0.5 1) (t 1    1) (t 0.75 0) h0 h1 h2
  , Triangle tm br bl (t 0.5 1) (t 0.75 0) (t 0.25 0) h0 h1 h2
  ]
  where
    h0 = fromIntegral c0 / 24
    h1 = fromIntegral c1 / 24
    h2 = fromIntegral c2 / 24
    tm = midpoint tl tr
    t = Point
