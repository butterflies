module Fuzz
  ( Fuzz()
  , empty
  , size
  , insert
  , lookup
  , elem
  , delete
  , toList
  , fromList
  ) where

import Prelude hiding (elem, lookup)
import Data.List (foldl', partition)
import Data.Maybe (isJust, listToMaybe)

data Fuzz a = Fuzz
  { _size   :: Integer
  , _insert :: a -> (Integer, Fuzz a)
  , _lookup :: Integer -> Maybe a
  , _elem   :: a -> Bool
  , _delete :: a -> Fuzz a
  , _toList :: [(Integer, a)]
  }

empty :: (a -> a -> Bool) -> Fuzz a
empty eq = fuzz eq 0 0 []

fromList :: (a -> a -> Bool) -> [a] -> Fuzz a
fromList eq = foldl' (\f a -> snd (insert f a)) (empty eq)

size :: Fuzz a -> Integer
size = _size

insert :: Fuzz a -> a -> (Integer, Fuzz a)
insert = _insert

lookup :: Fuzz a -> Integer -> Maybe a
lookup = _lookup

elem :: a -> Fuzz a -> Bool
elem = flip _elem

delete :: Fuzz a -> a -> Fuzz a
delete = _delete

toList :: Fuzz a -> [(Integer, a)]
toList = _toList

fuzz :: (a -> a -> Bool) -> Integer -> Integer -> [(Integer, a)] -> Fuzz a
fuzz eq next count xs = Fuzz
  { _size = count
  , _insert = \x -> case listToMaybe (filter (eq x . snd) xs) of
      Nothing -> (next, fuzz eq (next + 1) (count + 1) ((next, x) : xs))
      Just (n, _) -> (n, fuzz eq next count xs)
  , _lookup = \n -> case listToMaybe (filter ((== n) . fst) xs) of
      Nothing -> Nothing
      Just (_, x) -> Just x
  , _elem = \x -> isJust $ listToMaybe (filter (eq x . snd) xs)
  , _delete = \x -> let (ys, zs) = partition (eq x . snd) xs
                   in  fuzz eq next (count - toInteger (length ys)) zs
  , _toList = xs
  }
