module Colour
  ( colour
  ) where

import Control.Monad (liftM2)
import Data.List (partition)
import Data.Maybe (fromMaybe, listToMaybe)

import HalfPlane
import Trapezium
import Fuzz as F

p', q', hyp, adj, opp, d0, d1 :: Double
p' = 7
q' = 3
hyp = acosh ( (cos (pi/p') * cos (pi/q')) / (sin (pi/p') * sin (pi/q')) )
adj = acosh ( cos (pi/q') / sin (pi/p') )
opp = acosh ( cos (pi/p') / sin (pi/q') )
d0 = 2 * hyp + 2 * adj + 2 * opp
d1 = 2 * acosh ( cos (pi / 7) / sin (pi / 14) )

vertices :: [Trapezium] -> [(Int, Fuzz Point)]
vertices
  = zip [0..23]
  . map (F.fromList (equivalent 0))
  . concatMap (equivalenceClassesBy (equivalent d1))
  . equivalenceClassesBy (equivalent d0)
  . map snd
  . F.toList
  . F.fromList (equivalent 0)
  . liftM2 ($) [bTL, bFR, bFL]

colour' :: [(Int, Fuzz Point)] -> Trapezium -> Trapezium
colour' fs t = t{ bSpots = cTL, bWings = cFL, bBody = cFR }
  where
    cTL = go bTL
    cFL = go bFL
    cFR = go bFR
    go k = fromMaybe 12 $ listToMaybe [ c | (c, f) <- fs, k t `F.elem` f ]

colour :: [Trapezium] -> [Trapezium]
colour ts = let fs = vertices ts in map (colour' fs) ts

equivalent :: Double -> Point -> Point -> Bool
equivalent d p q = abs ((p `dist` q) - d) < 1e-3

equivalenceClasses :: Eq a => [a] -> [[a]]
equivalenceClasses = equivalenceClassesBy (==)

equivalenceClassesBy :: (a -> a -> Bool) -> [a] -> [[a]]
equivalenceClassesBy eq zs = go [] [] zs
  where
    go cs []     [] = cs
    go [] [] (x:xs) = go [[x]] [] xs
    go cs (o:os) [] = go ([o]:cs) [] os
    go (c:cs) os is = let f i = any (`eq` i) c
                          (is',os') = partition f is
                      in if null is'
                          then go (c:cs) (os' ++ os) []
                          else go ((is' ++ c) : cs) [] (os' ++ os)
    go [] (_:_) (_:_) = error "equivalenceClassesBy invariant violated"
