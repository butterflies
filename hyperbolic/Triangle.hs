module Triangle
  ( Triangle(..)
  , subtriangles
  ) where

import HalfPlane

data Triangle = Triangle{ tV0, tV1, tV2, tT0, tT1, tT2 :: Point, tH0, tH1, tH2 :: Double }
  deriving Show

subtriangles :: Triangle -> [Triangle]
subtriangles (Triangle v0 v1 v2 t0 t1 t2 h0 h1 h2) =
  [ Triangle v0  v01 v02 t0  t01 t02 h0 h1 h2
  , Triangle v1  v01 v12 t1  t01 t12 h0 h1 h2
  , Triangle v2  v02 v12 t2  t02 t12 h0 h1 h2
  , Triangle v01 v02 v12 t01 t02 t12 h0 h1 h2
  ]
  where
    v01 = midpoint v0 v1
    v02 = midpoint v0 v2
    v12 = midpoint v1 v2
    t01 = eMidpoint t0 t1
    t02 = eMidpoint t0 t2
    t12 = eMidpoint t1 t2
