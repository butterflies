{-# LANGUAGE TypeSynonymInstances #-}
module Main (main) where

import Control.Monad (when)
import Foreign hiding (rotate)
import Foreign.C (CUChar)
import Foreign.C.String
import Foreign.ForeignPtr (withForeignPtr)
import Graphics.Rendering.OpenGL.Raw
import Graphics.UI.GLUT hiding (rotate, translate, compileShader, RGBA, Triangle, Point)
import Data.Array.Repa.IO.DevIL
import Data.Array.Repa.Repr.ForeignPtr

import Paths_butterflies (getDataFileName)

import HalfPlane (Point(Point))
import Trapezium (toTriangles, neighbours, trapezium)
import Tesselate (tesselate)
import Colour (colour)
import Triangle (Triangle(Triangle), subtriangles)

swarm :: [Triangle]
swarm = concatMap subtriangles . concatMap toTriangles . colour $ tesselate (Point (-2) 0.01) (Point 2 4)

class GLPoke t where glPoke :: t -> Ptr t -> IO (Ptr t)
instance GLPoke t => GLPoke [t] where
  glPoke [] p = return p
  glPoke (x:xs) p = glPoke x (castPtr p) >>= glPoke xs . castPtr
instance GLPoke GLfloat where glPoke x p = poke p x >> return (advancePtr p 1)
instance GLPoke Double where
  glPoke x p = do
    p <- glPoke (realToFrac x :: GLfloat) (castPtr p)
    return (castPtr p)
instance GLPoke Point where
  glPoke (Point x y) p = do
    p <- glPoke x (castPtr p)
    p <- glPoke y p
    return (castPtr p)
instance GLPoke Triangle where
  glPoke (Triangle v0 v1 v2 t0 t1 t2 h0 h1 h2) p = do
    p <- glPoke [v0, t0] (castPtr p)
    p <- glPoke [h0, h1, h2] (castPtr p)
    p <- glPoke [v1, t1] (castPtr p)
    p <- glPoke [h0, h1, h2] (castPtr p)
    p <- glPoke [v2, t2] (castPtr p)
    p <- glPoke [h0, h1, h2] (castPtr p)
    return (castPtr p)

vert = unlines
  [ "#version 400 core"
  , "uniform mat4 mvp;"
  , "layout(location = 0) in vec2 vp0;"
  , "layout(location = 1) in vec2 tc0;"
  , "layout(location = 2) in vec3 hs0;"
  , "smooth out vec2 tc;"
  , "flat out vec3 hs;"
  , "void main() {"
  , "  vec4 vp = vec4(vp0, 0.0, 1.0) * mvp;"
  , "  gl_Position = vec4(vp.xy, 0.0, 1.0);"
  , "  tc = tc0;"
  , "  hs = hs0;"
  , "}"
  ]

frag = unlines
  [ "#version 400 core"
  , "uniform sampler2D tex;"
  , "uniform sampler1D pal;"
  , "smooth in vec2 tc;"
  , "flat in vec3 hs;"
  , "out layout(location = 0, index = 0) vec4 f;"
  , "void main() {"
  , "  vec4 c = vec4(0.0);"
  , "  for (int i = 0; i < 16; ++i) {"
  , "  for (int j = 0; j < 16; ++j) {"
  , "  vec2 t = tc + float(i)/16.0 * dFdx(tc) + float(j)/16.0 * dFdy(tc);"
  , "  vec4 w = texture(tex, t);"
  , "  if (w.a <= 0.5) {"
  , "    c += vec4(0.5, 0.5, 0.5, 1.0);"
  , "  } else if (w.r >= 0.5 && w.g >= 0.5 && w.b >= 0.5) {"
  , "    c += vec4(1.0, 1.0, 1.0, 1.0);"
  , "  } else if (w.r <= 0.5 && w.g <= 0.5 && w.b <= 0.5) {"
  , "    c += vec4(0.0, 0.0, 0.0, 1.0);"
  , "  } else if (w.r >= 0.5) {"
  , "    c += texture(pal, hs.r);"
  , "  } else if (w.g >= 0.5) {"
  , "    c += texture(pal, hs.g);"
  , "  } else if (w.b >= 0.5) {"
  , "    c += texture(pal, hs.b);"
  , "  } else {"
  , "    c = w;"
  , "  }}}"
  , "  f = c / 256.0;"
  , "}"
  ]

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "butterfly"
  loadTexture2D "butterfly.png"
  glActiveTexture gl_TEXTURE1
  loadTexture1D "palette.png"
  program <- compileProgram
  let count = length swarm * 3
      stride = (2 + 2 + 3) * 4
      bytes = count * fromIntegral stride
  allocaBytes bytes $ \p -> do
    glPoke swarm p
    vbo <- with 0 $ \q -> glGenBuffers 1 q >> peek q
    glBindBuffer gl_ARRAY_BUFFER vbo
    glBufferData gl_ARRAY_BUFFER (fromIntegral bytes) p gl_STATIC_DRAW
    vao <- with 0 $ \q -> glGenVertexArrays 1 q >> peek q
    glBindVertexArray vao
    glEnableClientState gl_VERTEX_ARRAY
    glVertexAttribPointer 0 2 gl_FLOAT (fromIntegral gl_FALSE) stride (plusPtr nullPtr (0 * 4))
    glVertexAttribPointer 1 2 gl_FLOAT (fromIntegral gl_FALSE) stride (plusPtr nullPtr (2 * 4))
    glVertexAttribPointer 2 3 gl_FLOAT (fromIntegral gl_FALSE) stride (plusPtr nullPtr (4 * 4))
    glEnableVertexAttribArray 0
    glEnableVertexAttribArray 1
    glEnableVertexAttribArray 2
  let l = -1.5
      r = 1.5
      t = 3
      b = 0
      n = -1
      f = 1
      ortho = [ 2 / (r - l), 0, 0, -(r + l) / (r - l)
              , 0, 2 / (t - b), 0, -(t + b) / (t - b)
              , 0, 0, 2 / (f - n), -(f + n) / (f - n)
              , 0, 0, 0, 1 ]
      ident = [ 1, 0, 0, 0, 0, 1, 0, -1, 0 ,0, 1, 0, 0, 0, 0, 1 ]
  glUseProgram program
  withArray ortho $ \p -> do
    loc <- withCString "mvp" $ glGetUniformLocation program
    glUniformMatrix4fv loc 1 (fromIntegral gl_FALSE) p
  withCString "tex" $ \p -> glGetUniformLocation program p >>= \loc -> glUniform1i loc 0
  withCString "pal" $ \p -> glGetUniformLocation program p >>= \loc -> glUniform1i loc 1
  glClearColor 0.5 0.5 0.5 1
  displayCallback $= do
    glClear gl_COLOR_BUFFER_BIT
    glDrawArrays gl_TRIANGLES 0 (fromIntegral count)
    swapBuffers
    reportErrors
  mainLoop

loadTexture2D f = do
  RGBA img <- getDataFileName f >>= runIL . readImage
  withForeignPtr (toForeignPtr img) $ \p -> do
    tex <- with 0 $ \q -> glGenTextures 1 q >> peek q
    glBindTexture gl_TEXTURE_2D tex
    glTexImage2D gl_TEXTURE_2D 0 (fromIntegral gl_RGBA) 1024 1024 0 gl_RGBA gl_UNSIGNED_BYTE p
    glTexParameteri gl_TEXTURE_2D gl_TEXTURE_MIN_FILTER (fromIntegral gl_LINEAR_MIPMAP_LINEAR)
    glTexParameteri gl_TEXTURE_2D gl_TEXTURE_MAG_FILTER (fromIntegral gl_LINEAR)
    glGenerateMipmap gl_TEXTURE_2D

loadTexture1D f = do
  RGBA img <- getDataFileName f >>= runIL . readImage
  withForeignPtr (toForeignPtr img) $ \p -> do
    tex <- with 0 $ \q -> glGenTextures 1 q >> peek q
    glBindTexture gl_TEXTURE_1D tex
    glTexImage1D gl_TEXTURE_1D 0 (fromIntegral gl_RGBA) 256 0 gl_RGBA gl_UNSIGNED_BYTE p
    glTexParameteri gl_TEXTURE_1D gl_TEXTURE_MIN_FILTER (fromIntegral gl_LINEAR_MIPMAP_LINEAR)
    glTexParameteri gl_TEXTURE_1D gl_TEXTURE_MAG_FILTER (fromIntegral gl_LINEAR)
    glGenerateMipmap gl_TEXTURE_1D
    
compileProgram = do
  program <- glCreateProgram
  compileShader program gl_VERTEX_SHADER vert
  compileShader program gl_FRAGMENT_SHADER frag
  glLinkProgram program
  debugProgram program
  return program

compileShader program t src = do
  shader <- glCreateShader t
  withCString src $ \srcp -> with srcp $ \srcpp -> glShaderSource shader 1 srcpp nullPtr
  glCompileShader shader
  glAttachShader program shader
  glDeleteShader shader

debugProgram program = do
  if program /= 0
    then do
      linked <- with 0 $ \p -> glGetProgramiv program gl_LINK_STATUS p >> peek p
      when (linked /= fromIntegral gl_TRUE) $ putStrLn "link failed"
      len <- with 0 $ \p -> glGetProgramiv program gl_INFO_LOG_LENGTH p >> peek p
      allocaBytes (fromIntegral len + 1) $ \p -> glGetProgramInfoLog program len nullPtr p >> pokeByteOff p (fromIntegral len) (0 :: CUChar) >> peekCString p >>= putStrLn
    else putStrLn "no program"
