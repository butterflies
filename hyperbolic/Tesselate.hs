module Tesselate
  ( tesselate
  ) where

import HalfPlane (Point(Point), dist)
import Trapezium

elemBy :: (t -> t -> Bool) -> t -> [t] -> Bool
elemBy eq x [] = False
elemBy eq x (y:ys)
  | eq x y = True
  | otherwise = elemBy eq x ys

closure :: (t -> Bool) -> (t -> t -> Bool) -> (t -> [t]) -> t -> [t]
closure p e f x = closure' [] [x]
  where
    closure' _ [] = []
    closure' old xs = xs ++ closure' (old ++ new) new
      where
        new = [ y | y <- concatMap f xs, p y, not (elemBy e y old) ]

tesselate :: Point -> Point -> [Trapezium]
tesselate (Point lx ly) (Point hx hy) = closure visible approxEq neighbours trapezium
  where
    approxEq s t = dist (origin s) (origin t) < 0.1
    visible = inBox . origin
    inBox (Point x y) = lx <= x && x <= hx && ly <= y && y <= hy
